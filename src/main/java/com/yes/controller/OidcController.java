package com.yes.controller;

import java.net.URI;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nimbusds.jwt.JWT;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.AuthorizationCodeGrant;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponseParser;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;

import net.minidev.json.JSONObject;

/**
 * Handles OIDC callback and result page display
 *
 * @author dkeijsers
 */
@Controller
public class OidcController {

	@CrossOrigin(origins = "*")
    @RequestMapping("/callback")
    public ModelAndView callback(@RequestParam(value = "code", required = false) String code, Model model,
                                 HttpSession session) throws Exception {
    	
    	System.out.println("+++++++++ Code erhalten: "+code);

    	String callbackUrl = "http://kinderarbeit.bnjmnrtl.com:8090/callback";
    	//String callbackUrl = "http://localhost:8090/callback";
    	
        // get code from auth request response
        AuthorizationCode authCode = new AuthorizationCode(code);

        // get token with client credentials and code
        TokenRequest tokenReq = new TokenRequest(
                new URI("http://test.yes.com/yes/token"),
                new ClientSecretBasic(new ClientID("nordlb"), new Secret("29dnsabdDSye")),
                new AuthorizationCodeGrant(authCode, new URI(callbackUrl)));
        HTTPRequest httpReq = tokenReq.toHTTPRequest();
        System.out.println("+++++ HTTPReq: " +httpReq.toString());
        
        HTTPResponse tokenHTTPResp = httpReq.send();
        OIDCTokenResponse accessTokenResponse = (OIDCTokenResponse) OIDCTokenResponseParser.parse(tokenHTTPResp);
        BearerAccessToken bearerToken = accessTokenResponse.getOIDCTokens().getBearerAccessToken();

        JWT idToken = accessTokenResponse.getOIDCTokens().getIDToken();
        System.out.println("++++++++ ID-Token:  " +       idToken.toString());

        // get user info
        UserInfoRequest userInfoReq = new UserInfoRequest(new URI("http://test.yes.com/yes/userinfo"), bearerToken);
        HTTPResponse userInfoHTTPResp = userInfoReq.toHTTPRequest().send();

        UserInfoSuccessResponse successResponse = (UserInfoSuccessResponse) UserInfoResponse.parse(userInfoHTTPResp);
        JSONObject claims = successResponse.getUserInfo().toJSONObject();
        String userInfo = claims.toJSONString();

        String transferId = session.getAttribute("transferId").toString();
        
        
        String message = claims.get("payment_"+transferId).toString();
        if ("ZAK".equalsIgnoreCase(message) || "BILL".equalsIgnoreCase(message) || "DEBIT".equalsIgnoreCase(message)) {
        	message = "Taschengeld wurde überwiesen";
        }
        else {
        	message = "Fehler beim Zahlen des Taschengelds";
        }
        
        session.setAttribute("message", message);
        session.setAttribute("idtoken", idToken.getJWTClaimsSet().toJSONObject());
        session.setAttribute("userinfo", userInfo);

     //   return new ModelAndView("redirect:/result");
        return new ModelAndView("redirect:http://kinderarbeit.bnjmnrtl.com/index.html#/bank-account-transfer-ok");
    }

    /**
     * "TRANSFER_ID_1234F", "Konto Kind 1", 3000, "EUR", "DE65500000000012305678"
     */
    @CrossOrigin(origins = "*")
    @RequestMapping("/zahlung")
    public ModelAndView zahlung(@RequestParam(value = "transferId", required = true) String transferId,
    		@RequestParam(value = "empfaenger", required = true) String empfaenger,
    		@RequestParam(value = "betrag", required = true) String betrag,
    		@RequestParam(value = "waehrung", required = true) String waehrung,
    		@RequestParam(value = "iban", required = false) String iban,
    		Model model, HttpSession session) throws Exception {
    	
    	session.setAttribute("transferId", transferId);
    	session.setAttribute("empfaenger", empfaenger);
    	session.setAttribute("betrag", betrag);
    	session.setAttribute("waehrung", waehrung.toUpperCase());
    	session.setAttribute("iban", iban.toUpperCase());
    	
    	System.out.println("++++++++ Pfad: " + session.getServletContext().toString());
    	
    	return new ModelAndView("zahlung");
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping("/result")
    public ModelAndView showResult() {
        return new ModelAndView("result");
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping("/info")
    public ModelAndView info() {
        return new ModelAndView("info");
    }
    
}
