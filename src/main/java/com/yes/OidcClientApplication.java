package com.yes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application context class
 *
 * @author dkeijsers
 */
@SpringBootApplication
public class OidcClientApplication {

    public static void main(String[] args) {

        // enable HTTPS calls
        System.setProperty("jsse.enableSNIExtension", "false");

        SpringApplication.run(OidcClientApplication.class, args);
    }
}
