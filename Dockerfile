FROM openjdk:8-jdk
COPY target/oidc-client-0.0.1-SNAPSHOT.jar /usr/local/bin

CMD ["java","-jar","/usr/local/bin/oidc-client-0.0.1-SNAPSHOT.jar"]